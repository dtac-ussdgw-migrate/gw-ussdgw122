package th.co.tac.cns.app.gw.ussdgw122.service;

import java.util.Properties;

import org.springframework.stereotype.Service;

import th.co.tac.cns.lib.call3rdparty.intl.service.constant.IntlServiceConst.FLAG;
import th.co.tac.cns.lib.call3rdparty.intl.service.soap.ws.common.checkblnc.CheckBalanceServiceCaller;
import th.co.tac.cns.lib.call3rdparty.intl.service.soap.ws.common.checkblnc.dto.ChckBlncResp;
import th.co.tac.cns.lib.common.exception.CNSException;
import th.co.tac.cns.lib.framework.gw.ussdgw.AbstractUSSDService;
import th.co.tac.cns.lib.framework.gw.ussdgw.model.USSDServiceResult;
import th.co.tac.cns.lib.framework.gw.ussdgw.util.USSDCachingManager;
import th.co.tac.cns.lib.framework.gw.ussdgw.util.USSDCommandParser;

@Service
public class USSD122Service extends AbstractUSSDService {

	public static final String TAG = "USSD122Service";
	private static final String CONF_RESP_DETL = "CommonCheckBalanceWs.RespDetl";

	@Override
	protected String getServiceName() {
		return TAG;
	}

	@Override
	protected USSDServiceResult doProcess(Properties params) throws CNSException {
		logger.info("### START PROCESS REQUEST ###");
		logger.info("process with input:" + params.toString());
		
		String commandForPerfLog = null;

		boolean isDefaultMessage = true; // Default lang is eng
		String language = params.getProperty("language");
		String subrNumber = params.getProperty("mobile");
		String imsi = params.getProperty("imsi");
		String userCode = params.getProperty("userCode");
		String remoteAddr = params.getProperty("remoteAddr");
		String input = params.getProperty("input");
		String sessionId = params.getProperty("sessionId");

		if (language.equalsIgnoreCase("Eng")) {
			language = "E";
		} else {
			isDefaultMessage = false;
			language = "T";
		}

		try {

			String[] inputData = USSDCommandParser.parseDataUSSD(input);

			if (inputData.length == 1) {
				// *122#
				this.logger.debug("Check Family Usage");
				commandForPerfLog = "*122#";
				USSDCachingManager.setFunctionId("1");

				ChckBlncResp chckBlncResp = null;
				CheckBalanceServiceCaller checkBalanceServiceCaller = new CheckBalanceServiceCaller();
				chckBlncResp = checkBalanceServiceCaller.checkBalance(userCode, language, FLAG.Y, FLAG.Y, input,
						config.getProperty(CONF_RESP_DETL));

				USSDServiceResult result = new USSDServiceResult();
				result.setSuccess(true);
				result.setSmsCode("SUCCESS_CHECK_USAGE");
				return result;

			} else {
				throw new CNSException(TAG, "99009", "Invalid USSD command");
			}

		} finally {
			if (USSDCachingManager.getFunctionId() == null) {
				USSDCachingManager.setFunctionId("0");
				commandForPerfLog = "INVALID_CMD";
			}

			logger.info("FUNC_ID[" + USSDCachingManager.getFunctionId() + "]|PERF_CMD[" + commandForPerfLog + "]");
		}

	}
}
