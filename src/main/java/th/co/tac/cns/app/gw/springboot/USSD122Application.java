package th.co.tac.cns.app.gw.springboot;

import org.springframework.boot.SpringApplication;

import th.co.tac.cns.lib.framework.springboot.apps.AbstractApplication;


public class USSD122Application extends AbstractApplication{
	
	public static void main(String[] args) {
		SpringApplication.run(USSD122Application.class, args);
	}
	
}
