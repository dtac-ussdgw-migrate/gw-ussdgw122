package th.co.tac.cns.app.gw.ussdgw122;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import th.co.tac.cns.app.gw.ussdgw122.service.USSD122Service;
import th.co.tac.cns.lib.framework.controller.AbstractHTTPController;
import th.co.tac.cns.lib.framework.gw.BLService;
import th.co.tac.cns.lib.framework.gw.ussdgw.util.USSDMessageParser;
import th.co.tac.cns.lib.framework.model.RequestDataInfo;

@RestController
@RequestMapping("/gw-ussdgw122")
public class USSD122RestController extends AbstractHTTPController {

	@Value("${app.name}")
	private String appName;
	
	@Autowired
	private USSD122Service blService;

	@Override
	protected String getServiceName() {
		return this.appName;
	}

	@Override
	protected BLService getBLService() {
		return this.blService;
	}
	
	@ResponseBody
    @RequestMapping(method = RequestMethod.POST, value = "/service", produces = "text/xml")
	public ResponseEntity<String> post(HttpServletRequest httpRequest) {
		return doRequest(httpRequest, RequestMethod.POST);
	}

	@Override
	protected String buildInternalServerErrorResponse(RequestDataInfo requestDataInfo, Throwable e) {
		e.printStackTrace();
		return USSDMessageParser.toXMLInternalErrorResponse();
	}
}
